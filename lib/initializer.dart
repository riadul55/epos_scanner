import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class Initializer {
  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();
    await _initStorage();
    // await _initGetConnect();
  }

  static Future<void> _initStorage() async {
    await GetStorage.init();
    Get.put(GetStorage());
  }

  // static Future<void> _initGetConnect() async {
  //   final connect = GetConnect();
  //   final url = ConfigEnvironments.getEnvironments()['url'];
  //   connect.baseUrl = url;
  //   connect.timeout = const Duration(seconds: 20);
  //   connect.httpClient.maxAuthRetries = 0;
  //
  //   connect.httpClient.addRequestModifier<dynamic>(
  //         (request) {
  //       // final storage = Get.find<GetStorage>();
  //       // final token = storage.read(StorageConstants.tokenAuthorization);
  //       // if (token != null) {
  //       //   request.headers['Authorization'] = 'Bearer $token';
  //       // }
  //       return request;
  //     },
  //   );
  //
  //   connect.httpClient.addResponseModifier(
  //         (request, response) async {
  //       if (response.statusCode == 401) {
  //         // final authDomainBinding = AuthRepositoryBinding();
  //         // await authDomainBinding.repository.logoutUser();
  //       }
  //     },
  //   );
  //
  //   Get.put(connect);
  // }
}