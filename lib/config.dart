class Business {
  static const String LAB = 'lab';
}

class ConfigEnvironments {
  static const String _currentBusiness = Business.LAB;
  static const List<Map<String, String>> _availableEnvironments = [
    {
      'env': Business.LAB,
      'url': '',
    },
  ];

  static Map<String, String> getEnvironments() {
    return _availableEnvironments.firstWhere((d) => d['env'] == _currentBusiness);
  }
}