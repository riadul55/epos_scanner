import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../routes/app_pages.dart';
import '../../values/themes/app_theme.dart';
import 'bindings/main_binding.dart';

class MainApp extends StatelessWidget {
  final String initialRoute;
  const MainApp({Key? key, required this.initialRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Epos Scanner",
      theme: AppTheme.lightThemeData,
      darkTheme: AppTheme.lightThemeData,
      initialBinding: MainBinding(),
      initialRoute: initialRoute,
      getPages: AppPages.routes,
    );
  }
}