import 'package:epos_scanner/app/data/local_db/tables/app_db.dart';
import 'package:epos_scanner/app/values/colors/app_colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/home_controller.dart';
import 'app_barcode_scanner_widget.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Epos Barcode scanner'),
        centerTitle: false,
        actions: [
          IconButton(
            icon: const Icon(
              Icons.clear,
              color: Colors.white,
            ),
            onPressed: () {
              // controller.clearBarcodes();
              // controller.showCode();
              controller.database.barcodeDao.deleteAll();
            },
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            flex: 1,
            child: AppBarcodeScannerWidget.defaultStyle(
              resultCallback: (val) {
                controller.getBarCode(val);
              },
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 6),
                child: _buildCodeList(context)),
          ),
        ],
      ),
      // bottomNavigationBar: ElevatedButton(
      //   child: const Text("Start Scan"),
      //   onPressed: () {
      //     Get.to(() =>
      //         );
      //   },
      // ),
    );
  }

  StreamBuilder<List<Barcode>> _buildCodeList(BuildContext context) {
    return StreamBuilder(
        stream: controller.database.barcodeDao.watchAllBarcode(),
        builder: (context, AsyncSnapshot<List<Barcode>> snapshot) {
          final codes = snapshot.data ?? [];
          return ListView.builder(
              itemCount: codes.length,
              itemBuilder: (_, index) {
                final barCode = codes[index];
                return ListTile(
                  title: Text(
                    barCode.value,
                    style: GoogleFonts.poppins(
                      color: AppColors.primaryText,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  leading: IconButton(
                    icon: const Icon(Icons.refresh),
                    onPressed: () {
                      // controller.database.deleteCode(barCode);
                    },
                  ),
                  trailing: IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () {
                      controller.database.barcodeDao.deleteCode(barCode);
                    },
                  ),
                  onTap: () {
                    Get.rawSnackbar(
                      messageText: Text(
                        "Copied!",
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                        ),
                      ),
                      backgroundColor: Colors.black,
                    );
                  },
                );
              });
        });
  }
}
