import 'package:get/get.dart';

import '../../../data/local_db/tables/app_db.dart';

class HomeController extends GetxController {
  late AppDb database;
  @override
  void onInit() {
    super.onInit();
    database=AppDb();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}


  void getBarCode(String code){
    final data = Barcode(
        value: code);
    database.barcodeDao.insertCode(data);
    // showCode();

  }

  void showCode() async {
    // String message=await database.barcodeDao.getAllBarcode();
    // print('\x1B[31m$message\x1B[0m');
  }
}
