// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'barcode.dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$BarcodeDaoMixin on DatabaseAccessor<AppDb> {
  $BarcodesTable get barcodes => attachedDatabase.barcodes;
  Future<int> deleteAllCodes() {
    return customUpdate(
      'delete from barcodes',
      variables: [],
      updates: {barcodes},
      updateKind: UpdateKind.delete,
    );
  }
}
