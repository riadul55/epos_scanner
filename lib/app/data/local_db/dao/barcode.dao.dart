import 'package:epos_scanner/app/data/local_db/tables/app_db.dart';
import 'package:moor_flutter/moor_flutter.dart';
import '../tables/barcodes.dart';

part 'barcode.dao.g.dart';

@UseDao(tables: [Barcodes],
    queries: {
    "deleteAllCodes": "delete from barcodes",
})
class BarcodeDao extends DatabaseAccessor<AppDb> with _$BarcodeDaoMixin {
  BarcodeDao(AppDb db) : super(db);

  Future deleteAll() {
    return deleteAllCodes();
  }

  Future<List<Barcode>> getAllBarcode() => select(barcodes).get();

  Stream<List<Barcode>> watchAllBarcode() => (select(barcodes)..orderBy(([
    (t) => OrderingTerm(expression: t.id, mode: OrderingMode.desc),
  ]))).watch();

  Future insertCode(Barcode code) => into(barcodes).insert(code);

  Future updateCode(Barcode code) => update(barcodes).replace(code);

  Future deleteCode(Barcode code) => delete(barcodes).delete(code);
}
