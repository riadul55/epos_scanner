import 'package:moor_flutter/moor_flutter.dart';


class Barcodes extends Table {
  IntColumn get id => integer().nullable().autoIncrement()();
  TextColumn get value => text()();
}