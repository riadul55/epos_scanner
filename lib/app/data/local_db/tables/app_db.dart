import 'package:epos_scanner/app/data/local_db/dao/barcode.dao.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'barcodes.dart';

part 'app_db.g.dart';

//flutter packages pub run build_runner build

@UseMoor(tables: [Barcodes], daos: [BarcodeDao])
class AppDb extends _$AppDb {
  AppDb()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: "db.sqlite", logStatements: true));

  @override
  int get schemaVersion => 1;
}
